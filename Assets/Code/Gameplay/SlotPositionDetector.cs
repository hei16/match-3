﻿using System.Collections.Generic;
using UnityEngine;

public static class SlotPositionDetector
{
	public static Slot GetBeneathSlot(Vector2Int position, Vector2Int measurement, Slot[,] slots)
	{
		if(position.x < 0 || position.y < 1)
		{
			return null;
		}

		return slots[position.x, position.y - 1];
	}

	public static List<Slot> GetNeighbourPositions(Vector2Int position, Vector2Int measurement, Slot[,] slots)
	{
		var positionsList = new List<Slot>();

		bool left = position.x == 0;
		bool bot = position.y == 0;
		bool right = position.x == measurement.x - 1;
		bool top = position.y == measurement.y - 1;

		if (left && bot)
		{
			positionsList.Add(slots[position.x + 1, position.y]);
			positionsList.Add(slots[position.x, position.y + 1]);
		}
		else if (right && bot)
		{
			positionsList.Add(slots[position.x - 1, position.y]);
			positionsList.Add(slots[position.x, position.y + 1]);
		}
		else if (right && top)
		{
			positionsList.Add(slots[position.x - 1, position.y]);
			positionsList.Add(slots[position.x, position.y - 1]);
		}
		else if (left && top)
		{
			positionsList.Add(slots[position.x + 1, position.y]);
			positionsList.Add(slots[position.x, position.y - 1]);
		}
		else if (bot)
		{
			positionsList.Add(slots[position.x - 1, position.y]);
			positionsList.Add(slots[position.x, position.y + 1]);
			positionsList.Add(slots[position.x + 1, position.y]);
		}
		else if (right)
		{
			positionsList.Add(slots[position.x, position.y - 1]);
			positionsList.Add(slots[position.x - 1, position.y]);
			positionsList.Add(slots[position.x, position.y + 1]);
		}
		else if (top)
		{
			positionsList.Add(slots[position.x - 1, position.y]);
			positionsList.Add(slots[position.x, position.y - 1]);
			positionsList.Add(slots[position.x + 1, position.y]);
		}
		else if (left)
		{
			positionsList.Add(slots[position.x, position.y + 1]);
			positionsList.Add(slots[position.x + 1, position.y]);
			positionsList.Add(slots[position.x, position.y - 1]);
		}
		else
		{
			positionsList.Add(slots[position.x + 1, position.y]);
			positionsList.Add(slots[position.x - 1, position.y]);
			positionsList.Add(slots[position.x, position.y + 1]);
			positionsList.Add(slots[position.x, position.y - 1]);
		}

		return positionsList;
	}
}
