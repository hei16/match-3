﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ItemsContainer")]
public class ItemsContainer : ScriptableObject
{
	public int ItemTypesCount => _items.Length;

	[SerializeField] private Item[] _items;

	public Item GetRandomItem()
	{
		return _items[Random.Range(0, _items.Length)];
	}

	public Item GetItem(int id)
	{
		if(id < _items.Length)
		{
			return _items[id];
		}

		return null;
	}

	public Item GetRandomItemExcept(int exceptID)
	{
		var items = new List<Item>();

		for (int i = 0; i < _items.Length; i++)
		{
			var item = _items[i];

			if(item.ID != exceptID)
			{
				items.Add(item);
			}
		}

		return items[Random.Range(0, items.Count)];
	}

	public Item GetRandomItemExcept(int[] exceptIDs)
	{
		int[,] results = new int[_items.Length, exceptIDs.Length];

		for (int i = 0; i < _items.Length; i++)
		{
			for (int j = 0; j < exceptIDs.Length; j++)
			{
				results[i, j] = _items[i].ID == exceptIDs[j] ? 1 : 0;
			}
		}

		List<Vector2> sums = new List<Vector2>();

		for (int i = 0; i < results.GetLength(0); i++)
		{
			sums.Add(new Vector2(i, 0));
			for (int j = 0; j < results.GetLength(1); j++)
			{
				var v = sums[i];
				v.y += results[i, j];
				sums[i] = v;
			}
		}

		var indexes = sums.FindAll(x => x.y == 0);
		var randomIndex = Random.Range(0, indexes.Count);
		var itemIndex = (int)indexes[randomIndex].x;

		return _items[itemIndex];
	}
}
