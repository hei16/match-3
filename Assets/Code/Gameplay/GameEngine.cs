﻿using DG.Tweening;
using UnityEngine;

public class GameEngine : MonoBehaviour
{
	public static bool AllowInput;

	public static GameEngine Instance;

	public static ItemsContainer ItemsContainer => Instance._itemsContainer;
	public static MaterialsContainer MaterialsContainer => Instance._materialsContainer;

	[SerializeField] private GameObject _levelPref;
	[SerializeField] private ItemsContainer _itemsContainer;
	[SerializeField] private MaterialsContainer _materialsContainer;
	[SerializeField] private UIController _uiController;

	private Level _level;
	private int _score;

	public Slot SelectedSlot { get; set; }

	private void Awake()
	{
		Instance = this;
	}

	public void StartGame(int measurement)
	{
		_score = 0;

		_uiController.UpdateScoreText(_score);

		AllowInput = true;

		var levelGO = (GameObject)Instantiate(_levelPref, Vector3.zero, Quaternion.identity);

		_level = levelGO.GetComponent<Level>();
		_level.SetMeasurement(measurement);
		_level.Init();
	}

	public void SwapSlotsItems(Slot slot1, Slot slot2, bool checkMatch)
	{
		AllowInput = false;

		_level.ColorsContainer.Remove(slot1);
		_level.ColorsContainer.Remove(slot2);

		var temp = slot1.Item;
		slot1.Item = slot2.Item;
		slot2.Item = temp;

		_level.ColorsContainer.Add(slot1);
		_level.ColorsContainer.Add(slot2);
		
		if(slot1.Item != null)
		{
			slot1.Item.transform.SetParent(slot1.transform);
		}
		
		if(slot2.Item != null)
		{
			slot2.Item.transform.SetParent(slot2.transform);
		}

		var s = DOTween.Sequence();

		s.Append(slot1.MoveAnimation());
		s.Join(slot2.MoveAnimation());

		s.Play().OnComplete(() =>
		{
			if (checkMatch)
			{
				AllowInput = true;

				var canMatch1 = _level.ColorsContainer.CheckMatch(slot1.ID);
				var canMatch2 = _level.ColorsContainer.CheckMatch(slot2.ID);

				if(!canMatch1 && !canMatch2)
				{
					SwapSlotsItems(slot2, slot1, false);
				}
			}
			else
			{
				AllowInput = true;
			}
		});
	}

	public void AddScore(int value)
	{
		_score += value;
		_uiController.UpdateScoreText(_score);
	}
}
