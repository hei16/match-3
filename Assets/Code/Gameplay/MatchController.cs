﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MatchController
{
	public static List<Slot> CheckColorMatch(List<Slot> slots)
	{
		List<Slot> slotsToDestroy = new List<Slot>();

		foreach (var s in slots)
		{
			var neighbourSlotsWithIdenticalItem = s.NeighbourSlots.FindAll(x => x.ID == s.ID);

			for (int i = 0; i < neighbourSlotsWithIdenticalItem.Count; i++)
			{
				var neighbourSlotsWithIdenticalItemOfNeighbour = neighbourSlotsWithIdenticalItem[i].NeighbourSlots.FindAll(x => x.ID == s.ID);

				for (int j = 0; j < neighbourSlotsWithIdenticalItemOfNeighbour.Count; j++)
				{
					if (neighbourSlotsWithIdenticalItemOfNeighbour[j] == s)
						continue;

					var potentialSlotsToDestroy = new List<Slot>();
					potentialSlotsToDestroy.Add(s);
					potentialSlotsToDestroy.Add(neighbourSlotsWithIdenticalItem[i]);
					potentialSlotsToDestroy.Add(neighbourSlotsWithIdenticalItemOfNeighbour[j]);

					if(SlotsAreTheSameRow(potentialSlotsToDestroy) || SlotsAreTheSameColumn(potentialSlotsToDestroy))
					{
						if (!ContainsAll(slotsToDestroy, potentialSlotsToDestroy))
						{
							slotsToDestroy.AddRange(potentialSlotsToDestroy);
						}
					}
				}
			}
		}

		return slotsToDestroy;
	}

	private static bool ContainsAll(List<Slot> list, List<Slot> smallerList)
	{
		for (int i = 0; i < smallerList.Count; i++)
		{
			if (!list.Contains(smallerList[i]))
				return false;
		}

		return true;
	}

	private static bool SlotsAreTheSameRow(List<Slot> slots)
	{
		var y = slots[0].Position.y;
		for (int i = 1; i < slots.Count; i++)
		{
			if (slots[i].Position.y != y)
				return false;
		}

		return true;
	}

	private static bool SlotsAreTheSameColumn(List<Slot> slots)
	{
		var x = slots[0].Position.x;
		for (int i = 1; i < slots.Count; i++)
		{
			if (slots[i].Position.x != x)
				return false;
		}

		return true;
	}
}
