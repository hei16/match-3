﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	[SerializeField] private Text _scoreText;

	[SerializeField] private GameObject _chooseLevelScreen;

	private void Start()
	{
		_scoreText.gameObject.SetActive(false);
	}

	public void UpdateScoreText(int score)
	{
		_scoreText.text = $"Score: {score}";
	}

	public void HandleChooseLevelButton(int levelMeasurement)
	{
		GameEngine.Instance.StartGame(levelMeasurement);
		_scoreText.gameObject.SetActive(true);
		_chooseLevelScreen.gameObject.SetActive(false);
	}
}
