﻿using UnityEngine;

[CreateAssetMenu(fileName = "New MaterialsContainer")]
public class MaterialsContainer : ScriptableObject
{
	public Material SlotDefault => _slotDefault;
	public Material SlotHightlight => _slotHighlight;

    [SerializeField] private Material _slotDefault;
	[SerializeField] private Material _slotHighlight;
}
