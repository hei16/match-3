﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ColorsContainer : MonoBehaviour
{
	public Action OnMatch;

	private Dictionary<int, List<Slot>> _colorsDictionary;

	public void Init(int colorsCount)
	{
		_colorsDictionary = new Dictionary<int, List<Slot>>();

		for (int i = 0; i < colorsCount; i++)
		{
			_colorsDictionary.Add(i, new List<Slot>());
		}
	}

	public bool CheckMatch(int id)
	{
		var matchedSlots = MatchController.CheckColorMatch(_colorsDictionary[id]);

		if(matchedSlots.Count > 0)
		{
			int score = 0;

			foreach (var s in matchedSlots)
			{
				score += 10;

				Remove(s);
				s.DestroyItem();
			}

			OnMatch?.Invoke();

			GameEngine.Instance.AddScore(score);

			return true;
		}

		return false;
	}

	public void SwitchActive(int id)
	{
		var list = _colorsDictionary[id];
		for (int i = 0; i < list.Count; i++)
		{
			list[i].SwitchEnable();
		}
	}

	public void Add(Slot slot)
	{
		if(slot.Item != null)
		{
			_colorsDictionary[slot.ID].Add(slot);
		}
	}

	public void Remove(Slot slot)
	{
		if(slot.Item != null)
		{
			_colorsDictionary[slot.ID].Remove(slot);
		}
	}
}
