﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour
{
	public int ID => Item.ID;
	public Vector2 Position { get; set; }
	public Item Item { get; set; }
	public Slot BeneathSlot { get; private set; }
	public List<Slot> NeighbourSlots { get; private set; }

	private MeshRenderer _renderer;

	private bool _hided;

	public void SwitchEnable()
	{
		_hided = !_hided;
		transform.DOKill();
		transform.DOScale(_hided ? 0f : 1f, .3f);
	}

	public void DestroyItem()
	{
		if(Item != null)
		{
			Item.Hide();
			Item = null;
		}
	}

	public void SwitchNeighbourSlots()
	{
		for (int i = 0; i < NeighbourSlots.Count; i++)
		{
			NeighbourSlots[i].SwitchEnable();
		}
	}

    public void Init(List<Slot> neighbourSlots, Slot beneathSlot)
	{
		_renderer = GetComponentInChildren<MeshRenderer>();
		_renderer.material = GameEngine.MaterialsContainer.SlotDefault;

		NeighbourSlots = neighbourSlots;
		BeneathSlot = beneathSlot;
	}

	public void PushItemOverSlot()
	{
		Item.transform.localPosition += new Vector3(0f, 1.5f, 0f);
	}

	public void InitItem(Item item)
	{
		Item = Instantiate(item);

		Item.transform.SetParent(transform);
		Item.transform.localPosition = Vector3.zero;
		Item.Init();
	}

	public Tween MoveAnimation()
	{
		if(Item != null)
		{
			Item.transform.DOKill();
			return Item.transform.DOLocalMove(Vector3.zero, 1f);
		}

		return null;
	}

	private void OnMouseDown()
	{
		if (!GameEngine.AllowInput)
			return;

		var clickedSlot = this;
		var currSelectedSlot = GameEngine.Instance.SelectedSlot;

		if(currSelectedSlot == null)
		{
			clickedSlot.HighLight();
		}
		else
		{
			if(clickedSlot == currSelectedSlot)
			{
				UnHighlight();
			}
			else
			{
				if (currSelectedSlot.NeighbourSlots.Contains(clickedSlot))
				{
					currSelectedSlot.UnHighlight();
					GameEngine.Instance.SwapSlotsItems(currSelectedSlot, clickedSlot, true);
				}
			}
		}
	}

	public void HighLight()
	{
		GameEngine.Instance.SelectedSlot = this;
		_renderer.material = GameEngine.MaterialsContainer.SlotHightlight;
	}

	public void UnHighlight()
	{
		GameEngine.Instance.SelectedSlot = null;
		_renderer.material = GameEngine.MaterialsContainer.SlotDefault;
	}
}
