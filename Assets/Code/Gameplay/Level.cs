﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Level : MonoBehaviour
{
	public ColorsContainer ColorsContainer => _colorsContainer;

	[SerializeField] private int _width;
	[SerializeField] private int _height;

	[SerializeField] private Slot _slotPref;

	private Slot[,] _slots;

	private ColorsContainer _colorsContainer;

	public void SetMeasurement(int measurement)
	{
		_width = measurement;
		_height = measurement;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			_colorsContainer.SwitchActive(0);
		}
		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			_colorsContainer.SwitchActive(1);
		}
		if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			_colorsContainer.SwitchActive(2);
		}
		if (Input.GetKeyDown(KeyCode.Alpha4))
		{
			_colorsContainer.SwitchActive(3);
		}
		if(Input.GetKeyDown(KeyCode.Space) && GameEngine.Instance.SelectedSlot != null)
		{
			GameEngine.Instance.SelectedSlot.SwitchNeighbourSlots();
		}
	}

	public void Init()
	{
		var colorsContainerGO = new GameObject("colorsContainer");
		colorsContainerGO.transform.SetParent(transform);
		_colorsContainer = colorsContainerGO.AddComponent<ColorsContainer>();
		_colorsContainer.Init(GameEngine.ItemsContainer.ItemTypesCount);
		_colorsContainer.OnMatch += () => StartCoroutine(CheckEmptySlots());

		CreateTable();
		InitSlots();
		ShowItemsInSlots();
	}

	private void CreateTable()
	{
		_slots = new Slot[_width, _height];

		var xOffset = _width * .5f;
		var yOffset = _height * .5f;

		var firstPosition = new Vector3(-.5f, -.5f);

		for (int i = 0; i < _width; i++)
		{
			var row = new GameObject($"row_{i}");
			row.transform.SetParent(transform);

			for (int j = 0; j < _height; j++)
			{
				var slotGO = (GameObject)Instantiate(_slotPref.gameObject);

				slotGO.name = $"slot_{i}_{j}";
				slotGO.transform.position = firstPosition + new Vector3(i * 1.5f - xOffset, j * 1.5f - yOffset);
				slotGO.transform.SetParent(row.transform);

				_slots[i, j] = slotGO.GetComponent<Slot>();
				_slots[i, j].Position = new Vector2(i, j);
			}
		}
	}

	private void InitSlots()
	{
		for (int i = 0; i < _width; i++)
		{
			for (int j = 0; j < _height; j++)
			{
				InitSlot(_slots[i, j], new Vector2Int(i, j));
			}
		}
	}

	private void InitSlot(Slot slot, Vector2Int position)
	{
		var beneathSlot = SlotPositionDetector.GetBeneathSlot(position, GetLevelMeasurement(), _slots);
		var neighbourSlots = SlotPositionDetector.GetNeighbourPositions(position, GetLevelMeasurement(), _slots);

		slot.Init(neighbourSlots, beneathSlot);

		Item item;

		if(position.x == 0 && position.y == 0)
		{
			item = GameEngine.ItemsContainer.GetRandomItem();
		}
		else
		{
			if (position.x == 0)
			{
				var beneathItemID = slot.BeneathSlot.ID;
				item = GameEngine.ItemsContainer.GetRandomItemExcept(beneathItemID);
			}
			else if(position.y == 0)
			{
				var leftItemID = _slots[position.x - 1, position.y].ID;
				item = GameEngine.ItemsContainer.GetRandomItemExcept(leftItemID);
			}
			else
			{
				var beneathItemID = slot.BeneathSlot.ID;
				var leftItemID = _slots[position.x - 1, position.y].ID;
				int[] itemIDs = { beneathItemID, leftItemID };
				item = GameEngine.ItemsContainer.GetRandomItemExcept(itemIDs);
			}
		}

		slot.InitItem(item);
		_colorsContainer.Add(slot);
	}

	private Vector2Int GetLevelMeasurement()
	{
		return new Vector2Int(_slots.GetLength(0), _slots.GetLength(1));
	}

	private void ShowItemsInSlots()
	{
		StopAllCoroutines();
		StartCoroutine(ShowItems());
	}

	private IEnumerator ShowItems()
	{
		for (int i = 0; i < _width; i++)
		{
			for (int j = 0; j < _height; j++)
			{
				_slots[i, j].Item.Show();

				yield return null;
				yield return null;
				yield return null;
				yield return null;
				yield return null;
			}
		}
	}

	IEnumerator CheckEmptySlots()
	{
		GameEngine.AllowInput = false;

		List<Slot> slotsList = new List<Slot>();

		for (int i = 0; i < _slots.GetLength(0); i++)
		{
			for (int j = 0; j < _slots.GetLength(1); j++)
			{
				slotsList.Add(_slots[i, j]);
			}
		}

		while (slotsList.Find(x => x.Item == null) != null)
		{
			foreach (var s in _slots)
			{
				if (s.Item == null)
				{
					GetItemFromTopSlot(s);
				}
			}
		}

		yield return new WaitForSeconds(1.1f);

		CheckMatch();

		GameEngine.AllowInput = true;
	}

	public void GetItemFromTopSlot(Slot slot)
	{
		if(slot.Position.y < _height - 1)
		{
			var topSlot = _slots[(int)slot.Position.x, (int)slot.Position.y + 1];

			if(topSlot.Item != null)
			{
				GameEngine.Instance.SwapSlotsItems(topSlot, slot, false);
			}
		}
		else
		{
			slot.InitItem(GameEngine.ItemsContainer.GetRandomItem());
			slot.PushItemOverSlot();
			slot.Item.Show();
			slot.MoveAnimation();
		}
	}

	public void CheckMatch()
	{
		foreach (var s in _slots)
		{
			ColorsContainer.CheckMatch(s.ID);
		}
	}
}
