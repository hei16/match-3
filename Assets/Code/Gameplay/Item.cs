﻿using UnityEngine;
using DG.Tweening;

public class Item : MonoBehaviour
{
	public int ID => _id;

	[SerializeField] private int _id;
	[SerializeField] private Transform _object;

	public void Init()
	{
		_object.localScale = Vector3.zero;
	}

	public void Show()
	{
		_object.localScale = Vector3.zero;

		_object.DOKill();
		_object.DOScale(.75f, .5f).SetEase(Ease.OutBack);
	}

	public void Hide()
	{
		_object.DOKill();
		_object.DOScale(0f, .5f).OnComplete(() => Destroy(_object.gameObject));
	}
}
